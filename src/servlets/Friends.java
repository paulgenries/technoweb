package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;


public class Friends extends HttpServlet {
	private String login1;
	private String login2;

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		login1 = req.getParameter("login1");
		login2 = req.getParameter("login2");

		// TODO peut etre print la cle ?
		JSONObject js;
		PrintWriter writer = res.getWriter();
		try {
			js = services.Friends.addFriends(login1, login2);
			writer.println(js.toString());

			switch (js.getInt("returnValue")) {
			case -1:
				writer.println("Erreur d’arguments passé au Web service, probleme de logins");
				writer.println(js.getString("messageValue"));
				break;
			case 0:
				writer.println(login1+" et "+ login2+" sont d�sormais des ami.e.s");
				writer.println(js.getString("messageValue"));
				break;
			case 100:
				writer.println("Probl�me JSON, erreur lors de l'ajout d'amis"); 
				writer.println(js.getString("messageValue"));
				break;
			case 1000:
				writer.println("SQL error, erreur lors de l'ajout d'amis");
				writer.println(js.getString("messageValue"));
				break;
			case 10000:
				writer.println("Erreur de JAVA,  erreur lors de l'ajout d'amis");
				writer.println(js.getString("messageValue"));
				break;
			default:
				writer.println("Erreur inconnue");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	protected void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		login1 = req.getParameter("login1");
		login2 = req.getParameter("login2");

		// TODO peut etre print la cle ?
		JSONObject js;
		PrintWriter writer = res.getWriter();
		try {
			js = services.Friends.removeFriend(login1, login2);
			switch (js.getInt("returnValue")) {
			case -1:
				writer.println("Erreur d'arguments pass� au Web service, probleme de logins");
				writer.println(js.getString("messageValue"));
				break;
			case 0:
				writer.println(login1+" et "+ login2+" ne sont d�sormais plus des ami.e.s");
				writer.println(js.getString("messageValue"));
				break;
			case 100:
				writer.println("Probl�me JSON, erreur lors de la suppression d'amis"); // TODO pas possible
				writer.println(js.getString("messageValue"));
				break;
			case 1000:
				writer.println("SQL error,  erreur lors de la suppression d'amis");
				writer.println(js.getString("messageValue"));
				break;
			case 10000:
				writer.println("Erreur de JAVA, erreur lors de la suppression d'amis");
				writer.println(js.getString("messageValue"));
				break;
			default:
				writer.println("Erreur inconnue");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}


	}
	
}
