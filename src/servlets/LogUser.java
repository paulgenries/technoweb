package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import tools.AuthentificationTools;
import tools.UserTools;

public class LogUser extends HttpServlet {
	private String login;
	private String mdp;
	private String cle;

	// TODO rajouter des cas de case genre 1,2 comme dans les services

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		login = req.getParameter("login");
		mdp = req.getParameter("mdp");

		// TODO peut etre print la cle ?
		
		JSONObject js;
		PrintWriter writer = res.getWriter();
		try {
			js = services.LogUser.logInUser(login, mdp);
			writer.println(js.toString() + " " + login);
			switch (js.getInt("returnValue")) {
			case -1:
				writer.println("Erreur d’arguments passé au Web service, probleme d'identifiant ou de mot de passe ");
				writer.println(js.getString("messageValue"));
				break;
			case 0:
				writer.println("User identifié");
				writer.println(js.getString("messageValue"));
				break;
			case 100:
				writer.println("Problème JSON, user non identifié"); // TODO pas possible
				writer.println(js.getString("messageValue"));
				break;
			case 1000:
				writer.println("SQL error, user non identifié");
				writer.println(js.getString("messageValue"));
				break;
			case 10000:
				writer.println("Erreur de JAVA, user non identifié");
				writer.println(js.getString("messageValue"));
				break;
			default:
				writer.println("Erreur inconnue");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	protected void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		cle = req.getParameter("cle");
		JSONObject js;
		PrintWriter writer = res.getWriter();

		js = services.LogUser.logoutUser(cle);
		writer.println(js.toString());
		try {
			switch (js.getInt("returnValue")) {
			case -1:
				writer.println("Erreur d’arguments passé au Web service, user non logout ");
				break;
			case 0:
				writer.println("User logout");
				break;
			case 100:
				writer.println("Problème JSON, user non logout");
				break;
			case 1000:
				writer.println("SQL error, user non logout");
				break;
			case 10000:
				writer.println("Erreur de JAVA, user non logout");
				break;
			default:
				writer.println("Erreur inconnue");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
