package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import tools.UserTools;

public class User extends HttpServlet {
	private String nom;
	private String prenom;
	private String mdp;
	private String login;

	// TODO rajouter des cas de case genre 1,2 comme dans les services
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// TODO creer un if pour les modifications au lieu de la creation en fonction
		// des parametres input dans la requete POST (creer tools
		// correspondants...)
		login = req.getParameter("login");
		nom = req.getParameter("nom");
		prenom = req.getParameter("prenom");
		mdp = req.getParameter("mdp");
		JSONObject js;
		PrintWriter writer = res.getWriter();

		js = services.User.createUser(login, nom, prenom, mdp);
		writer.println(js.toString());
		try {
			switch (js.getInt("returnValue")) {
			case -1:
				writer.println("Erreur d’arguments passé au Web service, user non crée ");
				break;
			case 0:
				writer.println("User crée");
				break;
			case 100:
				writer.println("Problème JSON, user non crée");
				break;
			case 1000:
				writer.println("SQL error, user non créé");
				break;
			case 10000:
				writer.println("Erreur de JAVA, user non crée");
				break;
			default:
				writer.println("Erreur inconnue");

			}

		} catch (JSONException e) { // TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	protected void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		login = req.getParameter("login");

		JSONObject js;
		PrintWriter writer = res.getWriter();
		try {
			js = services.User.deleteUser(login);
			switch (js.getInt("returnValue")) {
			case -1:
				writer.println("Erreur d’arguments passé au Web service, user non supprimé ");
				break;
			case 0:
				writer.println("User supprimé");
				break;
			case 100:
				writer.println("Problème JSON, user non supprimé");
				break;
			case 1000:
				writer.println("SQL error, user non supprimé");
				break;
			case 10000:
				writer.println("Erreur de JAVA, user non supprimé");
				break;
			default:
				writer.println("Erreur inconnue");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
