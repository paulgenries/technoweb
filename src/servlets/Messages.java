package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class Messages {

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String login = req.getParameter("login");
		String message = req.getParameter("message");
		
		JSONObject js;
		PrintWriter writer = res.getWriter();
		try {
			js = services.Messages.createMessage(login, message);
			writer.println(js.toString() + " " + login);
			switch (js.getInt("returnValue")) {
			case -1:
				writer.println("Erreur d’arguments passé au Web service, probleme d'identifiant ou de message ");
				writer.println(js.getString("messageValue"));
				break;
			case 0:
				writer.println("Message créé");
				writer.println(js.getString("messageValue"));
				break;
			case 100:
				writer.println("Problème JSON, message non créé"); // TODO pas possible
				writer.println(js.getString("messageValue"));
				break;
			case 1000:
				writer.println("SQL error, message non créé");
				writer.println(js.getString("messageValue"));
				break;
			case 10000:
				writer.println("Erreur de JAVA, message non créé");
				writer.println(js.getString("messageValue"));
				break;
			default:
				writer.println("Erreur inconnue");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
 
}
