package services;

import java.sql.Connection;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import bd.Database;
import tools.ServicesTools;
import tools.UserTools;

public class User {
	public static JSONObject createUser(String login,String nom,String prenom,String mdp) {
		JSONObject user = new JSONObject();
		Connection c;
		try {
			c = Database.getMySQLConnection();
			user.put("returnValue", 0);
			if(login==null || nom==null || prenom==null || mdp==null) {
				c.close();
				return ServicesTools.serviceRefused(1,"Un des parametres est vide (null)");
			}
			if(UserTools.userExist(login,c)) {
				c.close();
				return ServicesTools.serviceRefused(2,"L'utilisateur existe déjà"); 
			}
			UserTools.insertUser(login,nom,prenom,mdp,c);
			c.close();
			return ServicesTools.serviceAccepted();
		} catch (JSONException e){
			return ServicesTools.serviceRefused(300, "A voir"); //TODO 
		} catch(SQLException e) {
			return ServicesTools.serviceRefused(301, "A voir"); //TODO
		} 
	}
	
	public static JSONObject deleteUser(String login) {
		Connection c;
		try {
			c = Database.getMySQLConnection();
			if(login==null) {
				return ServicesTools.serviceRefused(1,"Un des paramètres est vide (null)");
			}
			
			if(!UserTools.userExist(login,c)) {
				return ServicesTools.serviceRefused(2,"L'utilisateur n'existe pas"); 
			}
			UserTools.deleteUser(login,c);
			return ServicesTools.serviceAccepted();
		} catch (JSONException e){
			return ServicesTools.serviceRefused(300, "A voir"); //TODO
		} catch(SQLException e) {
			return ServicesTools.serviceRefused(301, "A voir"); //TODO
		} 
	}
		
}
