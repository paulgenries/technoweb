package services;

import java.sql.Connection;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import bd.Database;
import tools.AuthentificationTools;
import tools.ServicesTools;
import tools.UserTools;

public class LogUser {
	//TODO RAJOUTER UN PUT("key",key) dans le login et le logout a tous les return?
	

	public static JSONObject logInUser(String login, String pwd) {
		try {

			Connection c = Database.getMySQLConnection();

			if ((login == null) || (pwd == null)) {
				
				return ServicesTools.serviceRefused(1, "Un des paramètres est vide (null)");
			}

			// on verifie que le user exist
			boolean is_user = UserTools.userExist(login, c);
			if (!is_user)
				return ServicesTools.serviceRefused(1, "Unknown user " + login);

			// on verifie que c'est le bon password
			boolean password_ok = AuthentificationTools.checkPassword(login, pwd, c);

			if (!password_ok) {
				return ServicesTools.serviceRefused(2, "Bad password " + login);
			}
			// On creer une nouvelle session pour l'utilisateur
			String key = AuthentificationTools.insertSession(login, false, c);
			c.close();
			return ServicesTools.serviceAccepted().put("key", key);

		} catch (JSONException e) {
			return ServicesTools.serviceRefused(100, "JSON Problem " + e.getMessage()); // TODO e.getMessage() ne marche
																						// pas
		} catch (SQLException e) {
			return ServicesTools.serviceRefused(300, "A voir");
		}
	}

	
	  public static JSONObject logoutUser(String cle) {
		  try { 
		      Connection c = Database.getMySQLConnection();
		  
			  boolean isValidKey = AuthentificationTools.keyExist(cle,c); 
			  if (!isValidKey) return ServicesTools.serviceRefused(1, "Unknown key " + cle);
			  AuthentificationTools.deleteSession(cle, c); 
			  return ServicesTools.serviceAccepted(); //TODO sans doute rajoute 
		  } catch (JSONException e) { 
			  return ServicesTools.serviceRefused(100, "JSON Problem "+ e.getMessage()); // TODO e.getMessage() ne marche // pas 
		  } catch (SQLException e) { 
			  return ServicesTools.serviceRefused(300, "A voir"); 
		  }
	 }
		 
}
