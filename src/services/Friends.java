package services;

import java.sql.Connection;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import bd.Database;
import tools.FriendsTools;
import tools.ServicesTools;
import tools.UserTools;
public class Friends {
	public static JSONObject addFriends(String login1,String login2) {
		Connection c;
		try {
			c = Database.getMySQLConnection();
			if(login1==null || login2==null) {
				return ServicesTools.serviceRefused(1,"Un des param�tres est vide (null)");
			}
			if(!UserTools.userExist(login1,c)||!UserTools.userExist(login2, c)) {
				return ServicesTools.serviceRefused(2,"Un des utilisateurs n'existe pas"); 
			}
			if(FriendsTools.checkFriends(login1,login2,c)) {
				return ServicesTools.serviceRefused(2,"L'amiti� existe dej�");
			}
			FriendsTools.addFriend(login1,login2,c);
			c.close();
			return ServicesTools.serviceAccepted();
		} catch (JSONException e){
			return ServicesTools.serviceRefused(300, "A voir"); //TODO
		} catch(SQLException e) {
			return ServicesTools.serviceRefused(301, "A voir"); //TODO
		} 
	}
	public static JSONObject removeFriend(String login1,String login2) {
		JSONObject user = new JSONObject();
		Connection c;
		try {
			c = Database.getMySQLConnection();
			user.put("returnValue", 0);
			if(login1==null || login2==null) {
				return ServicesTools.serviceRefused(1,"Un des param�tres est vide (null)");
			}
			
			if(!UserTools.userExist(login1,c)||!UserTools.userExist(login2, c)) {
				return ServicesTools.serviceRefused(2,"L'utilisateur n'existe pas"); 
			}
			
			// TODO y'a pas un !checkFriend?
			FriendsTools.removeFriend(login1,login2,c);
			c.close();
			return ServicesTools.serviceAccepted();
		} catch (JSONException e){
			return ServicesTools.serviceRefused(300, "A voir"); //TODO
		} catch(SQLException e) {
			return ServicesTools.serviceRefused(301, "A voir"); //TODO
		} 
	}
		
}
