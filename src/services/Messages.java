package services;

import java.sql.Connection;
import java.sql.SQLException;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.DB;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import bd.Database;
import tools.MessagesTools;
import tools.ServicesTools;
import tools.UserTools;

public class Messages {
	public static JSONObject createMessage(String login,String message) throws JSONException {
		MongoDatabase db = Database.getMongoDBConnection();
		MongoCollection<Document> coll = db.getCollection("messages");
		Connection c;
		try {
			c = Database.getMySQLConnection();
			
			if(login==null || message == null) {
				c.close();
				return ServicesTools.serviceRefused(1,"Un des paramètres est vide (null)");
			}
			
			if(UserTools.userExist(login,c)) {
				c.close();
				return ServicesTools.serviceRefused(2,"L'utilisateur existe déjà"); 
			}
			MessagesTools.envoieMessage(login, message, coll);
			c.close();
			return ServicesTools.serviceAccepted();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ServicesTools.serviceRefused(301, "A voir"); //TODO
		}

	
	}
}
