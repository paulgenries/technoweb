package tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class AuthentificationTools {
	public static String cle2;
	// TODO A VERIF
	public static boolean checkPassword(String login, String pwd, Connection c) throws SQLException {
		Statement stm = c.createStatement();
		String req = "SELECT * FROM USER WHERE login="+login; // TODO
		ResultSet rs = stm.executeQuery(req);
		rs.next();
		if (pwd.compareTo(rs.getString("pwd")) == 0) {
			stm.close();
			return true;
		}
		stm.close();
		return false;
	}

	public static String insertSession(String login, boolean pooling, Connection c) throws SQLException { // A	
		PreparedStatement pr = c.prepareStatement("INSERT INTO CLES VALUES(?,?,now())");
		String cle = createKey(c);
		pr.setString(1,cle);
		pr.setString(2,login);
		int rs = pr.executeUpdate();
		return cle;

	}

	private static String createKey(Connection c) throws SQLException {
		boolean b = true;
		Statement stm = c.createStatement();
		String cle = "";
		while (b) {
			cle = Integer.toString((int) (Math.random() * 1000));
			String req = "SELECT cle FROM CLES";
			ResultSet rs = stm.executeQuery(req);
			while (rs.next()) {
				if (rs.getString("cle").compareTo(cle) == 0) {
					break;
				}
			}
			b = rs.next();
		}
		cle2=cle;
		stm.close();
		return cle;
	}
	
	public static boolean keyExist(String cle,Connection c) throws SQLException {
		Statement stm=c.createStatement();
		ResultSet rs=stm.executeQuery("SELECT * FROM CLES WHERE cle="+cle);
		while(rs.next()) {
			stm.close();
			return true;
		}
		stm.close();
		return false;
	}

	public static boolean deleteSession(String cle, Connection c) throws SQLException {
		PreparedStatement pr = c.prepareStatement("DELETE FROM CLES WHERE cle="+cle);
		int res = pr.executeUpdate();
		return (res==0);
	}


}
