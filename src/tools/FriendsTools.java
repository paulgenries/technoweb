package tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FriendsTools {
	

	public static boolean addFriend(String login1, String login2, Connection c) throws SQLException {
		Statement stm=c.createStatement();
		int res=stm.executeUpdate("INSERT INTO FRIENDS VALUES("+login1+","+login2+")");
		return res==0;
	}

	public static boolean removeFriend(String login1, String login2, Connection c) throws SQLException {
		Statement stm=c.createStatement();
		int res=stm.executeUpdate("DELETE FROM FRIENDS WHERE USER1="+login1+" AND USER2="+login2);
		return res==0;
		
	}

	public static boolean checkFriends(String login1, String login2,Connection c) throws SQLException {
		Statement stm = c.createStatement();
		String req = "SELECT * FROM FRIENDS WHERE USER1="+login1+" AND USER2="+login2; 
		ResultSet rs = stm.executeQuery(req);
		while(rs.next()) {
			stm.close();
			return true;
		}
		stm.close();
		return false;
	}

}
